import socket

Terminator = "\n"  # Set the line terminator to LF

def check_connection(ip_address, port):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(2)  # Set a short timeout for the connection attempt
            s.connect((ip_address, port))
        return True
    except Exception as e:
        print(f"Connection error: {e}")
        return False

def send_scpi_command(ip_address, port, command, timeout=5):
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(timeout)  # Set the timeout for the recv operation
            s.connect((ip_address, port))
            
            # Append LF to the command and send
            s.sendall((command + Terminator).encode())
            
            response = s.recv(1024).decode()
            return response
    except socket.timeout:
        print("Timeout: No response from the power supply.")
        return None
    except Exception as e:
        print(f"Error: {e}")
        return None

if __name__ == "__main__":
    power_supply_ip = "192.168.169.115"
    power_supply_port = 1026  # Specify the correct port for your power supply

    if check_connection(power_supply_ip, power_supply_port):
        print("Connected to the power supply.")
        
        # Take user input for SCPI command
        scpi_command = input("Enter the SCPI command: ")
        
        response = send_scpi_command(power_supply_ip, power_supply_port, scpi_command)

        if response:
            print(f"Response from power supply: {response}")
