# Install IOC support modules




Pull the repository inside the EPICS installation folder.

Choose the support modules you would like to install using the modules.yml file.

Modify the paths inside the RELEASE.local to match your EPICS installation and support modules location.

Run install_modules.sh inside the EPICS folder, support directory has to exist.

## Prerequisites
```
 apt-get install curl -y
 curl -L https://github.com/mikefarah/yq/releases/download/v4.13.4/yq_linux_amd64 -o /usr/local/bin/yq && chmod +x /usr/local/bin/yq
```
