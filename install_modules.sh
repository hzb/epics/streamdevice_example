#!/bin/bash


# Define the input file
MODULES_FILE="modules.yml"
#MODULES_FILE="/tmp/modules.yml"

# Check if the file exists
if [ ! -f "$MODULES_FILE" ]; then
    echo "Error: $MODULES_FILE not found."
    exit 1
fi

export SUPPORT=/home/marcelb/EPICS/support

# Load modules from the YAML file
while IFS=": " read -r key value || [ -n "$key" ]; do
    if [[ $key == "repo" ]]; then
        repo=$value
    elif [[ $key == "tag" ]]; then
        tag=$value
    elif [[ $key == "folder_name" ]]; then
        folder_name=$value
    elif [[ $key == "dbd" ]]; then
        dbd=$value
        # Perform actions here with the extracted values
	mkdir ${SUPPORT}/${folder_name}
        echo "Cloning $repo with tag $tag into folder $folder_name"
        git clone --depth 1 --recursive --branch "$tag" "$repo" "${SUPPORT}/${folder_name}"
        cd ${SUPPORT}/${folder_name}
	make -C ${SUPPORT}/${folder_name}
    fi
done < <(yq eval '.modules | to_entries | .[] | .value | to_entries | .[] | .key + ": " + .value' $MODULES_FILE)

